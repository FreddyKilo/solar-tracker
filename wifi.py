import network

from credentials import Wifi
from output_interface import *


def connect():
    wlan = network.WLAN(network.STA_IF)
    # wlan.disconnect()

    #  Attempt wifi connection if not already connected
    if not wlan.isconnected():
        wlan.active(True)
        wlan.connect(Wifi.ssid(), Wifi.password())
        print_info('Attempting connection to WiFi', end='')

        #  Try to connect until connection is made or 10 seconds have passed
        led = LED_BUILTIN()
        for i in range(10):
            led.slow_blink()  # One second per blink
            print('.', end='')
            if wlan.isconnected():
                print_info('\nIP address: ' + wlan.ifconfig()[0])
                break

    print()
    return wlan.isconnected()
