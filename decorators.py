import json
import time


def loop(func):
    def looper():
        while True:
            func()
            time.sleep(1)

    return looper


def wait(seconds=0):
    def waiter(func):
        def wait_wrapper():
            func()
            time.sleep(seconds)
        return wait_wrapper

    return waiter


def responsebody(func):
    def body(arg1):
        return json.loads(func(arg1).split('\r\n\r\n')[1])

    return body


def responseheader(func):
    def header(arg1, arg2):
        return json.loads(func(arg1, arg2).split('\r\n\r\n')[0])

    return header
