import json
import socket


class HTTP:
    def __init__(self):
        self.payload = ''
        self.response = ''
        self.response_body = ''

    @staticmethod
    def __connect(url, port):
        s = socket.socket()
        try:
            address_info = socket.getaddrinfo(url, port)
            ip_address = address_info[0][-1][0]
            s.connect((ip_address, port))
        except Exception as error:
            print(error)

        return s

    def get(self, url, port=80):
        sock = self.__connect(url.split('/')[0], port)
        host, path = url.split('/', 1)
        sock.send(bytes('GET /%s HTTP/1.0\r\nHost: %s\r\n\r\n' % (path, host), 'utf8'))
        self.response = ''
        while True:
            data = sock.recv(100)
            if data:
                self.response += str(data, 'utf8')
            else:
                break

        sock.close()
        return self.response

    def post(self):
        pass

    @property
    def response_body(self):
        return json.loads(self.response.split('\r\n\r\n')[1])

    @response_body.setter
    def response_body(self, value):
        self._response_body = value


class URL:
    __solar_tracker_domain_name = 'solartracker.ddns.net'
    SOLAR_POSITION = __solar_tracker_domain_name + '/solar-position/coordinates/33.4484,-112.0740'
    SUNRISE = __solar_tracker_domain_name + '/sunrise/coordinates/33.4484,-112.0740'
    SUNSET = __solar_tracker_domain_name + '/sunset/coordinates/33.4484,-112.0740'
    SOLAR_TRACKER_SERVER_PORT = 5000
