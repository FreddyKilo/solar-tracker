import math
import sys

from machine import Pin, PWM, Signal
from utime import sleep_ms

from pinout import *


class LED:
    def __init__(self, pin_number, invert=False):
        self.__led = Pin(pin_number, Pin.OUT)
        self.__invert = invert

    def start_up(self):
        pwm = PWM(self.__led)
        pwm.freq(120)
        for i in range(12):
            self.__pulse(pwm)
        pwm.deinit()

    def fast_blink(self):
        self.__blink(20, 480, 1)

    def slow_blink(self):
        self.__blink(20, 980, 1)

    def heart_beat(self):
        self.__blink(20, 100, 2)
        sleep_ms(1760)

    def show_error(self, blink_count):
        sleep_ms(1000)
        self.__blink(500, 500, blink_count)
        sleep_ms(1000)

    def __blink(self, time_on, time_off, iterations):
        led = Signal(self.__led, invert=self.__invert)
        for i in range(iterations):
            led.on()
            sleep_ms(time_on)
            led.off()
            sleep_ms(time_off)

    def __pulse(self, pwm):
        for i in range(20):
            pwm.duty(int(math.sin(i / 10 * math.pi) * 500 + 500))
            sleep_ms(20)


class LED_BUILTIN(LED):
    ACTIVE_LOW_PLATFORMS = {
        "esp8266": NodeMCU.ESP_LED
    }

    ACTIVE_HIGH_PLATFORMS = {}

    def __init__(self):
        if sys.platform in self.ACTIVE_LOW_PLATFORMS:
            super().__init__(pin_number=self.ACTIVE_LOW_PLATFORMS[sys.platform], invert=True)
            return

        super().__init__(pin_number=self.ACTIVE_HIGH_PLATFORMS[sys.platform], invert=False)


class Error:
    WIFI_NOT_CONNECTED = 3
    MODEM_NOT_CONNECTED = 4
    NO_RESPONSE_FROM_SERVER = 5
    INFINITE = sys.maxsize


def print_info(message, end='\n'):
    print('[INFO] ' + message, end=end)


def print_error(message, end='\n'):
    print('[ERROR] ' + message, end=end)
