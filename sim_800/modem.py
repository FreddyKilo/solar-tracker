from utime import time

from output_interface import *

led = LED_BUILTIN()

# This is intended for use with a RPi (pins 8-TX and 10-RX)
# SERIAL = serial.Serial("/dev/ttyAMA0", 19200)


def reset():
    print_info('Resetting modem', end='')
    reset_pin = Pin(Wemos.D1, Pin.OUT)
    reset_pin.on()
    sleep_ms(100)
    reset_pin.off()
    sleep_ms(100)
    reset_pin.on()

    for x in range(6):  # wait seconds for sim_800 to startup
        print('.', end='')
        led.slow_blink()
    print()


def send_command(cmd, wait, success="OK", error="ERROR"):
    # print helpful feedback with wait time
    print_info('AT command: ' + cmd + ' (' + str(wait) + ' second wait)')
    # SERIAL.write(cmd)
    timeout = time() + wait
    response = 'Empty response'

    while True:

        if time() > timeout:
            break
        # else:
        # data = SERIAL.read(64)  # read response
        # if data is not None:
        #     data_str = ''.join([chr(b) for b in data])  # convert byte array to string
        #     if success or fail in data_str:
        #         response = data_str
        #         break

    if success in response:
        return True
    elif error in response:
        print_error('"' + error + '" in response: ' + cmd)
        print(response)
        return False
    else:
        print_error("Timeout: " + cmd)
        return False


def disconnect():
    # C0 - Disconnect / shutdown sim_800 connection
    if not send_command("AT+CIPCLOSE=1\r\n", 4, "CLOSE OK"):
        return False
    return True


def connect():
    reset()

    # C1 - Shutdown sim_800 before bringing it back up. Should wait about 30 sec
    if not send_command("AT+CIPSHUT\r\n", 3, "SHUT OK"):
        return False

    # C2 - Check GPRS status
    if not send_command("AT+CGATT?\r\n", 10, "OK"):
        return False

    # C3 - Set sim_800 mode
    if not send_command("AT+CIPMUX=1\r\n", 2, "OK"):
        return False

    # C4 - Set APN
    if not send_command("AT+CSTT=\"hologram\"\r\n", 2, "OK"):
        return False

    # C5 - Bring up wireless connection
    if not send_command("AT+CIICR\r\n", 85, "OK"):
        return False

    # C6 - Get local IP address
    if not send_command("AT+CIFSR\r\n", 2, "."):
        return False

    # C7 - Start inbound server
    if not send_command("AT+CIPSERVER=1,4010\r\n", 2, "SERVER OK"):
        return False

    return True

# def sendMessage(message):
#     # format message string
#     fullMessage = format_msg(message, DEVICE_KEY)
#
#     # C8 - Start hologram TCP connection
#     if not send_command("AT+CIPSTART=1,\"TCP\",\"" + ip() + "\",\"" + port() + "\"\r\n", 75, "OK", "FAIL"):
#         return False
#
#     # C9 - Set message length
#     msgLength = len(fullMessage)
#     if not send_command("AT+CIPSEND=1," + str(msgLength) + "\r\n", 5, ">"):
#         return False
#
#     # C10 - Send string to server
#     if not send_command(fullMessage, 60, "OK", "FAIL"):
#         return False
#
#     return True


# def sendResponse(responseMsg="RECEIVE OK"):
#     # C11 - Send a message back to server after receiving data
#     if not send_command("AT+CIPSEND=0," + str(len(responseMsg)) + "\r\n", 5, ">"):
#         return False
#
#     # C12 - Send string to server
#     if not send_command(responseMsg, 60, "SEND OK"):
#         return False
#
#     return True


# ########################################################
# print("### PROJECT STARTUP #############################")
# ########################################################
#
# # UART send "AT\r\n"
# # uart = busio.UART(D4, D3, baudrate=19200)
#
# sleep(2)
#
# # reset sim_800
# print("Modem RST ~45 seconds")
# reset()
#
# STARTUP = True
#
# ########################################################
# print("### MODEM CONFIGURE #############################")
# ########################################################
#
# while STARTUP:
#
#     # C13 - Check UART communication
#     if not send_command("AT\r\n", 3, "OK"):
#         break
#
#     # C14 - Set cell modules baud rate
#     if not send_command("AT+IPR=19200\r\n", 5, "OK"):
#         break
#
#     # C15 - Check if SIM is readable
#     if not send_command("AT+CPIN?\r\n", 5, "OK"):
#         break
#
#     # C16 - Set SMS to text mode
#     if not send_command("AT+CMGF=1\r\n", 10, "OK"):
#         break
#
#     print("### MODEM CONFIGURE SUCCESSFUL ###")
#     MODEM_CONFIG = True
#     break
#
# ########################################################
# print("### NETWORK CONNECT #############################")
# ########################################################
#
# if MODEM_CONFIG:
#
#     if connect():
#         print("### NETWORK CONNECT SUCCESSFUL ###")
#         NET_CONNECT = True
#
# ########################################################
# print("### SEND MESSAGE ################################")
# ########################################################
#
# if NET_CONNECT:
#
#     if sendMessage("Yay, We did it!"):
#         print("### SEND MESSAGE SUCCESSFUL ###")
#
#     disconnect()

# ########################################################
# print("### RECEIVE MESSAGE #############################")
# ########################################################

# while True:
#     data = uart.read(64)  # check for any inbound data
#
#     if data != None and "+RECEIVE" in data:
#         datastr = ''.join([chr(b) for b in data])  # convert bytearray to string
#         print(datastr)
#         sendResponse()
#
#     sleep(5)
