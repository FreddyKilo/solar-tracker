# import esp

# import wifi
from decorators import *
# from output_interface import *
# from sim_800 import modem
from the_internet import HTTP, URL

# led = LED_BUILTIN()
UPDATE_INTERVAL = 10000000  # microseconds


# def ready():
#     led.start_up()  # Give time to manually open up a serial monitor
#     print_info('System platform: ' + sys.platform)
#     pass


# def set():
#     if wifi.connect():
#         print_info('Connected to WiFi!')
#         return
#
#     print_error("Could not connect to WiFi\n\tAre you using the correct SSID and password?")
#     led.show_error(Error.WIFI_NOT_CONNECTED)
#
#     if modem.connect():
#         print_info('Modem connection obtained!')
#         return
#
#     print_error("Could not obtain a wireless connection with modem")
#     led.show_error(Error.MODEM_NOT_CONNECTED)
#
#     print_info("Entering ESP deep sleep mode")
#     esp.deepsleep(UPDATE_INTERVAL)


@loop
def go():
    http = HTTP()
    http.get(URL.SUNRISE, port=URL.SOLAR_TRACKER_SERVER_PORT)
    print(http.response_body)
    http.get(URL.SUNSET, port=URL.SOLAR_TRACKER_SERVER_PORT)
    print(http.response_body)
    http.get(URL.SOLAR_POSITION, port=URL.SOLAR_TRACKER_SERVER_PORT)
    print(http.response_body)
    time.sleep(2)
    # led.heart_beat()
    # esp.deepsleep(UPDATE_INTERVAL)
