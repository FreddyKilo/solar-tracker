@echo off

ampy --port COM4 put ../credentials.py
echo credentials.py deployed!
echo;

ampy --port COM4 put ../decorators.py
echo decorators.py deployed!
echo;

ampy --port COM4 put ../esp_app.py
echo esp_app.py deployed!
echo;

ampy --port COM4 put ../main.py
echo main.py deployed!
echo;

ampy --port COM4 put ../output_interface.py
echo output_interface.py deployed!
echo;

ampy --port COM4 put ../pinout.py
echo pinout.py deployed!
echo;

ampy --port COM4 put ../servo.py
echo servo.py deployed!
echo;

ampy --port COM4 put ../the_internet.py
echo the_internet.py deployed!
echo;

ampy --port COM4 put ../wifi.py
echo wifi.py deployed!
echo;

echo Files on the device:
ampy --port COM4 ls
echo;

echo Don't forget to push the reset button to start application!