from machine import Pin, PWM


class Servo:
    def __init__(self, channel):
        self.angle = None
        self.servo = PWM(Pin(channel), freq=50)

    def set_angle(self, angle):
        self.servo.duty(self.map_duty(angle))
        self.angle = angle

    def get_angle(self):
        return self.angle

    @staticmethod
    def map_duty(value):
        # TODO: figure out what these values should be
        in_min = 0
        in_max = 0
        out_min = 0
        out_max = 0
        return (value - in_min) * (out_max - out_min) / (in_max - in_min) + out_min
